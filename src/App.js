import avatar from './assets/images/48.jpg';
import Style from './App.module.css'


function App() {
  return (
    <div className={Style.devcamp}>
      <div className={Style.devcampWrapper}>
        <img className={Style.devcampAvatar} alt="User avatar" src={avatar}></img>
        <div className={Style.devcampQuote}>
          <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
        </div>
        <p className={Style.devcampName}>
          Tammy Stevens <span> Front end Developer</span>
        </p>
      </div>
    </div>

  );
}

export default App;
